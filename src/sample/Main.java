package sample;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.util.Date;

public class Main extends Application {
    private float width = 300;
    private float height = 300;
    private float radius = 75;
    private static Group g;
    private static Watch w;

    @Override
    public void start(Stage primaryStage) {
        Group root = new Group();
        g=root;

        Scene scene = new Scene(root, width, height);

        primaryStage.setTitle("Clock!");
        primaryStage.setScene(scene);
        primaryStage.show();
        this.clear();

        Thread d = new Thread(() -> {
        long mils = System.currentTimeMillis();
            while (true) {
                try {
                    Thread.sleep(1000);
                    mils = System.currentTimeMillis();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if (w != null)
                    w.setTime(new Date(mils));
            }


        });

        d.start();
    }

    private void clear() {
        Button b = new Button("Analog");
        b.setLayoutX(150);
        b.setLayoutY(270);
        Button b2 = new Button("Digital");
        b2.setLayoutX(225);
        b2.setLayoutY(270);

        b.setOnAction(action -> {
            clear();
            w = new AnalogWatch(width, height, radius);
            w.render();
        });

        b2.setOnAction(action -> {
            clear();
            w = new DigitalWatch();
            w.render();
        });

        getGroup().getChildren().clear();
        getGroup().getChildren().add(b);
        getGroup().getChildren().add(b2);
    }

    static Group getGroup() {
        return g;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
