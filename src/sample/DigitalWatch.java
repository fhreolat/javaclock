package sample;

import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.text.Font;

import java.text.SimpleDateFormat;

public class DigitalWatch extends Watch {
    private Label clock = new Label();

    DigitalWatch() {
        super();
        Main.getGroup().getChildren().add(clock);
        clock.setLayoutX(100);
        clock.setLayoutY(100);
        clock.setFont(new Font("Roboto", 30));
    }

    @Override
    protected void render() {
        Platform.runLater(() -> clock.setText(new SimpleDateFormat("HH:mm:ss").format(calendar.getTime())));
    }
}
