package sample;

import java.util.Calendar;
import java.util.Date;

abstract class Watch {
    private Date time = new Date();
    Calendar calendar = Calendar.getInstance();

    void setTime(Date time) {
        this.time = time;
        calendar.setTime(time);
        this.render();
    }

    protected abstract void render();
}
