package sample;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

import java.util.Calendar;

public class AnalogWatch extends Watch {

    private AnalogPointer seconds;
    private AnalogPointer minutes;
    private AnalogPointer hours;

    AnalogWatch(double width, double height, double radius) {

        Circle c = new Circle(width / 2, height / 2, radius);
        c.setStroke(Color.rgb(0, 0, 0));
        c.setFill(Color.rgb(255, 255, 255));

        seconds = new AnalogPointer(width / 2, height / 2, radius);
        minutes = new AnalogPointer(width / 2, height / 2, radius/1.3);
        hours = new AnalogPointer(width / 2, height / 2, radius/2.5);

        Main.getGroup().getChildren().add(c);
        Main.getGroup().getChildren().add(seconds);
        Main.getGroup().getChildren().add(minutes);
        Main.getGroup().getChildren().add(hours);
    }


    @Override
    protected void render() {
        double secs = calendar.get(Calendar.MILLISECOND)/1000d+calendar.get(Calendar.SECOND);
        seconds.render(secs*100d/60d);

        double mins = calendar.get(Calendar.MINUTE);
        minutes.render(mins*100/60);

        double hrs = calendar.get(Calendar.HOUR);
        hours.render(hrs*100/12);
    }
}
