package sample;

import javafx.scene.shape.Line;

class AnalogPointer extends Line {
    private double radius;
    AnalogPointer(double xStart, double yStart, double radius) {
        super(xStart, yStart, xStart, yStart);
        this.radius = radius;
    }

    void render(double percentage) {
        double x = Math.sin(percentage * (Math.PI / 2d) / 25d);
        double y = Math.cos(percentage * (Math.PI / 2d) / 25d);

        setEndX(this.getStartX() + x * radius);
        setEndY(this.getStartY() - y * radius);
    }
}
